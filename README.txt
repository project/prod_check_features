Production Check Features

The Production Check Features module extends the Production check/monitor
project with the ability to check for the status of features on remote sites.
This is useful when you maintain many sites and use features in the deployment
process. With this module enabled you will be able to see if all features are
still in the default state. If not, appropriate action can be taken.

Installation
Enable the module on the monitoring and to be monitored sites.
